mod solution;

use std::collections::HashMap;

use solution::*;

struct PostfixConvertor {
    result : Vec<String>
}

impl PostfixConvertor {
    pub fn transform(expression: &dyn Expression) -> String {
        let mut visitor = Self::new();
        expression.accept(&mut visitor);
        visitor.consume()
    }
    fn new() -> Self {
        PostfixConvertor { result: Vec::new() }
    }
    fn consume(self) -> String { self.result.join(" ") }
}

impl Visitor for PostfixConvertor {
    fn visit_const(&mut self, cst: &Const) {
        self.result.push(format!("{}", cst.value()));
    }

    fn visit_var(&mut self, var: &Variable) {
        self.result.push(format!("{}", var.name()));
    }

    fn visit_sum(&mut self, sum: &Sum) {
        sum.left().accept(self);
        sum.right().accept(self);
        self.result.push(format!("+"));
    }

    fn visit_product(&mut self, sum: &Product) {
        sum.left().accept(self);
        sum.right().accept(self);
        self.result.push(format!("*"));
    }
}

fn main() {
    let c = Const::new(27);
    let v = Variable::new("a".to_string());

    let s = Sum::new(c.clone(), v.clone());
    let p = Product::new(c.clone(), v.clone());

    let exp = Product::new(s.clone(), p.clone());

    let values = HashMap::from([("a".to_string(), 42)]);
    match Evaluate::transform(exp.as_ref(), &values) {
        Ok(value) => println!("{}", value),
        Err(Error::UnboundVariable) => println!("Missing variable."),
        Err(Error::UnexpectedEmptyStack) => println!("Stack empty, that was not expected!"),
        Err(_) => println!("Other error.")
    }

    let a = Variable::new("test");
    let mut values: HashMap<_, &dyn Expression> = HashMap::new();
    values.insert("a".to_string(), a.as_ref());
    let exp = Substitute::transform(exp.as_ref(), &values);
    println!("{}", PostfixConvertor::transform(exp.as_ref()));
}
