use std::collections::HashMap;
use std::ops::Deref;
use std::rc::Rc;

type RcExp = Rc<dyn Expression>;
type VariableHashMap = HashMap<String, NumConst>;
type NumConst = i64;

#[derive(Clone)]
pub struct Const {
    value : NumConst,
}

impl Const {
    pub fn new(val: NumConst) -> RcExp {
        Rc::new(Const { value : val })
    }

    pub fn value(&self) -> NumConst {
        self.value
    }
}

#[derive(Clone)]
pub struct Variable {
    name: String,
}

impl Variable {
    pub fn new<S>(val: S) -> RcExp where S: Into<String>{
        Rc::new(Variable { name : val.into() })
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }
}

#[derive(Clone)]
pub struct Sum {
    left: RcExp,
    right: RcExp,
}

impl Sum {
    pub fn new(left: RcExp, right: RcExp) -> RcExp {
        Rc::new(Sum {
            left: left,
            right: right
        })
    }

    pub fn left(&self) -> RcExp {
        self.left.clone()
    }

    pub fn right(&self) -> RcExp {
        self.right.clone()
    }
}

#[derive(Clone)]
pub struct Product {
    left: RcExp,
    right: RcExp,
}

impl Product {
    pub fn new(left: RcExp, right: RcExp) -> RcExp {
        Rc::new(Product {
            left: left,
            right: right
        })
    }

    pub fn left(&self) -> RcExp {
        self.left.clone()
    }

    pub fn right(&self) -> RcExp {
        self.right.clone()
    }
}

#[derive(Debug)]
pub enum Error {
    NotImplemented,
    UnboundVariable,    
    UnexpectedEmptyStack,
}

pub trait Visitor {
    fn visit_const(&mut self, cst: &Const);
    fn visit_var(&mut self, var: &Variable);
    fn visit_sum(&mut self, sum: &Sum);
    fn visit_product(&mut self, pro: &Product);
}

pub trait Expression {
    fn accept(&self, visitor: &mut dyn Visitor);

    fn clone_rc(&self) -> Rc<dyn Expression>;
}

impl Expression for Const {
    fn accept(&self, visitor: &mut dyn Visitor) {
        visitor.visit_const(self);
    }

    fn clone_rc(&self) -> Rc<dyn Expression> {
        Rc::new(self.clone())
    }
}

impl Expression for Variable {
    fn accept(&self, visitor: &mut dyn Visitor) {
        visitor.visit_var(self);
    }

    fn clone_rc(&self) -> Rc<dyn Expression> {
        Rc::new(self.clone())
    }
}

impl Expression for Sum {
    fn accept(&self, visitor: &mut dyn Visitor) {
        visitor.visit_sum(self);
    }

    fn clone_rc(&self) -> Rc<dyn Expression> {
        Rc::new(self.clone())
    }
}

impl Expression for Product {
    fn accept(&self, visitor: &mut dyn Visitor) {
        visitor.visit_product(self);
    }

    fn clone_rc(&self) -> Rc<dyn Expression> {
        Rc::new(self.clone())
    }
}

pub struct Evaluate {
    stack: Vec<Result<NumConst, Error>>,
    values: VariableHashMap,
}

impl Evaluate {
    pub fn new(values: &VariableHashMap) -> Evaluate {
        Evaluate {
            stack: Vec::new(),
            values: values.clone(),
        }
    }
    pub fn transform(expr: &dyn Expression, values: &VariableHashMap) -> Result<NumConst, Error> {
        let mut visitor = Self::new(values);

        expr.deref().accept(&mut visitor);
        visitor.stack.pop().unwrap()
    }
}

impl Visitor for Evaluate {
    fn visit_const(&mut self, cst: &Const) {
        self.stack.push(Ok(cst.value()))
    }

    fn visit_var(&mut self, var: &Variable) {
        match self.values.get(&var.name()) {
            Some(val) => {
                self.stack.push(Ok(*val));
            },
            None => {
                self.stack.push(Err(Error::UnboundVariable));
            }
        }
    }

    fn visit_sum(&mut self, sum: &Sum) {
        sum.left().accept(self);
        sum.right().accept(self);

        let val1 : NumConst;
        let val2 : NumConst;

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val1 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val2 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        self.stack.push(Ok(val1 + val2));
    }

    fn visit_product(&mut self, product: &Product) {
        product.left().accept(self);
        product.right().accept(self);

        let val1 : NumConst;
        let val2 : NumConst;

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val1 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val2 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        self.stack.push(Ok(val1 * val2));
    }
}

pub struct Substitute {
    stack: Vec<Result<RcExp, Error>>,
    values: HashMap<String, Rc<dyn Expression>>,
}

impl Substitute {
    pub fn new() -> Substitute {
        Substitute {
            stack: Vec::new(),
            values: HashMap::new(),
        }
    }

    pub fn transform(expr: &dyn Expression, values: &HashMap<String, &dyn Expression>) -> RcExp {
        let mut visitor = Self::new();

        for (name, exp) in values {
            visitor.values.insert(name.to_string(), exp.clone_rc());
        }
        
        expr.deref().accept(&mut visitor);
        visitor.stack.pop().unwrap().ok().unwrap()
    }
}

impl Visitor for Substitute {
    fn visit_const(&mut self, cst: &Const) {
        self.stack.push(Ok(cst.clone_rc()))
    }

    fn visit_var(&mut self, var: &Variable) {
        match self.values.get(&var.name()) {
            Some(val) => {
                self.stack.push(Ok(val.clone_rc()))
            },
            None => {
                self.stack.push(Err(Error::UnboundVariable));
            }
        }
    }

    fn visit_sum(&mut self, sum: &Sum) {
        sum.left().accept(self);
        sum.right().accept(self);

        let val1 : RcExp;
        let val2 : RcExp;

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val2 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val1 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        self.stack.push(Ok(Sum::new(val1, val2)));
    }

    fn visit_product(&mut self, product: &Product) {
        product.left().accept(self);
        product.right().accept(self);

        let val1 : RcExp;
        let val2 : RcExp;

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val2 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        match self.stack.pop() {
            Some(val) => {
                match val {
                    Ok(nval) => {
                        val1 = nval;
                    },
                    Err(err) => {
                        self.stack.push(Err(err));
                        return;
                    }
                }
            },
            None => {
                self.stack.push(Err(Error::UnexpectedEmptyStack));
                return;
            }
        }

        self.stack.push(Ok(Product::new(val1, val2)));
    }
}
